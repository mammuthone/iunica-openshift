var request = require('request');

var reqBA = function (req) {
	this.jar = request.jar();
	//this.followRedirect = false;
	this.auth = {};
	this.auth.user = req.params.user;
	this.auth.pass = req.params.pass;
	this.auth.sendImmediately = false;
	this.headers = { 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0' };
	return(this)
}


var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

module.exports.reqBA = reqBA;
module.exports.toType = toType;
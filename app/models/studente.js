var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var CareerSchema = new Schema({
	cod: String,
	tipo: String,
	titolo: String,
	stato: String,
	link : String	
})

var ExamSchema = new Schema({
	id : Number,
	anno : String,
	cod: String,
	nomeCorso : String,
	crediti: Number,
	passed: Boolean,
	frequenza: String,
	voto: String,
	dataReg : String
})

var StudenteSchema   = new Schema({
	name: String,
	careers: [CareerSchema],
	libretto : [ExamSchema],
	totCrediti : Number,
	mediaPond : String,
	mediaArit: String,
	corso : String
});

module.exports = mongoose.model('Studente', StudenteSchema);
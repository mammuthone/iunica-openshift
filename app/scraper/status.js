var cheerio = require('cheerio');

var statusStudente = function(html) {
	var $ = cheerio.load(html)
	var s = {};
	s.name = $('#sottotitolo-menu-principale dl dt').text();
	s.photo = 'https://webstudenti.unica.it/esse3/' + $('#sottotitolo-menu-principale dl dd img').attr('src');			
	s.mailAteneo = $('#gu-hpstu-boxDatiPersonali dl:nth-child(1) dd:nth-child(12) description:nth-child(1)').text();
	s.mailPersonale = $('#gu-hpstu-boxDatiPersonali dl:nth-child(1) dd:nth-child(5) description:nth-child(1)').text();
	console.log(s.mailPersonale,'-----------------------------');
	s.logout = "https://webstudenti.unica.it/esse3/auth/"+ $('#menu-open ul:nth-child(2) li:nth-child(1) a').attr('href');
	s.appelli = {};
	s.appelli.disp = '';
	s.appelli.disp = $('#gu-homepagestudente-tablePanelControl tr:nth-child(3) td:nth-child(2)').text().toString();
	var iunica = {}
	iunica.appelli= [];
	iunica.appelli = s.appelli.disp.trim().slice(0,2);
	s.appelli.disp = parseInt(iunica.appelli[0].trim());
	s.appelli.pren = $('#gu-homepagestudente-tablePanelControl tr:nth-child(4) td:nth-child(2)').text().toString();
	console.log('----------------------------------------')
	console.log(s.appelli.pren);
	iunica.pren= [];
	try {
	iunica.pren = s.appelli.pren.trim().slice(0,2);
	s.appelli.pren = parseInt(iunica.pren[0].trim());

	}
	catch (error) {
		console.log(error);
		s.appelli.pren = 0;
	}
	//#gu-textStatusStudente > b:nth-child(1)
	s.aa = $('#gu-textStatusStudente b:nth-child(1)').text().replace(/(\r\n|\n|\r)/gm,"");
	s.annoRegolamento = $('#gu-textStatusStudente b:nth-child(2)').text().replace(/(\r\n|\n|\r)/gm,"");
	s.statoCarriera =$('#gu-textStatusStudente b:nth-child(3)').text().replace(/(\r\n|\n|\r)/gm,"");
	s.corso = $('#gu-textStatusStudenteCorsoFac-text-link2').text().replace(/(\r\n|\n|\r)/gm,"");
	s.facolta = $('#gu-textStatusStudenteCorsoFac-text-link4').text().replace(/(\r\n|\n|\r)/gm,"");
	s.percorso = $('#gu-textStatusStudenteCorsoFac-text-link6').text().replace(/(\r\n|\n|\r)/gm,"");
	s.durata = $('#gu-boxStatusStudenteIscriz1 p:nth-child(1) b:nth-child(1) description:nth-child(1)').text().replace(/(\r\n|\n|\r)/gm,"").trim();
	s.annoDiCorso = $('#gu-boxStatusStudenteIscriz1 p:nth-child(1) b:nth-child(2) description:nth-child(1)').text().replace(/(\r\n|\n|\r|\t)/gm,"");
	s.immatricolazione = $('#gu-textStatusStudenteImma b:nth-child(1)').text().replace(/(\r\n|\n|\r)/gm,"");
	s.esamiRegistrati = $('dl.record-riga:nth-child(2) dd:nth-child(2) description:nth-child(1)').text();
	s.mediaAritmetica = $('dl.record-riga:nth-child(2) dd:nth-child(4) description:nth-child(1)').text();
	var mA = s.mediaAritmetica.split('/');
	s.mediaAritmetica = mA[0];
	s.mediaPonderata = $('dl.record-riga:nth-child(2) dd:nth-child(6) description:nth-child(1)').text();
	var mP = s.mediaPonderata.split('/');
	s.mediaPonderata = mP[0];
	return(s)
}




module.exports.statusStudente = statusStudente;

var express = require('express');
var session = require('express-session');
//var oauthserver = require('oauth2-server');

var app = express();
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var EventEmitter = require("events").EventEmitter;


var	reqBasicA = require('./app/constructors'),
	fs = require('fs'),
	connect = require('connect'),
	toType = require('./app/constructors'),
	cookieParser = require('cookie-parser'),
	//session = require('express-session'),
	fs = require('fs'),
	util = require('util'),
	events = require('events'),
	https = require('https'),
	request = require('request'),
	path = require('path'),
	cheerio = require('cheerio'),
	status = require('./app/scraper/status.js'),
	//mongoose = require('mongoose'),
	//config variables and db
	//db = require('./config/db.js'),
	//conf = require('./config/conf.js'),
	//Studente = require('./app/models/studente');

var sessID;

//mongoose.connect(db.url);

require('request-debug')(request);

// ####### UN-COMMENT THESE LINES TO ENABLE HTTPS ON SERVER
/*
https.createServer({
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem')
    }, app);
*/

//Configuration of the server
/*
app.oauth = oauthserver({
  model: {}, // See below for specification 
  grants: ['password'],
  debug: true
});
app.use(cookieParser('dr zaius doctor zaius'));
app.use(session({
	resave: false,
	saveUninitialized: true,
	secret: 'forzavecchiocuorerossoblu',
}));
*/

app.use(express.static('public'));
app.use(express.static('pictures'));

app.engine('ejs', require('ejs').renderFile);
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views')
app.set('view engine', 'html')

//app.all('/oauth/token', app.oauth.grant());



var server = app.listen(port);
var io = require('socket.io').listen(server);
var ee = new EventEmitter();


ee.on("esse3Complete", function (data) {
	console.log("esse3 retrieved");
	console.log(data.name);
	io.sockets.emit('finish');
});

var options = {};
/*
io.set('authorization', function (data, accept) {
	//console.log(handshakeData.headers['cookie']);
	if (data.headers.cookie) {
		data.cookie = cookieParser(data.headers.cookie);
		data.sessionID = data.cookie['connect.sid'];
	} else {
		return callback('No cookie transmitted.', false);
	}
	accept(null, true); // error first callback style 
});
*/

ee.on('sessionID', function(d){
	sessID = d;
	console.log('event emitter req.id:', sessID)
})

io.sockets.on('connection', function(socket){
	console.log('socket: ', socket.id, ' room: ', sessID);
	socket.join(sessID);
	io.to(sessID).emit('update', 5);
	socket.on('create', function(){
	});

})


//uncaught exception handler:
process.on('uncaughtException', function(err) {
  console.error(err.stack);
});

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

router.get('/:user/:pass', function (req, res, next) {
	console.log('loggin with credentials');
	console.log('richiesta API : ', req.sessionID);
	ee.emit('sessionID', req.sessionID)
	request('https://webstudenti.unica.it/esse3/auth/Logon.do', reqBasicA.reqBA(req), function (err, res1, html) {
		if (err) {
			console.log(err);
			return;
		}
		var error = {};

		var DOMsize = res1.headers['content-length'];
		console.log('DOM size: ', DOMsize);

		var $ = cheerio.load(html);
		if (res1.statusCode == 503) {
			error.down = true;
			console.log('il server di s3 è down');
			res.jsonp(error);
			ee.emit("esse3Complete", esse3);
			res1.end;
			return;
		}
		if (DOMsize == 103) {
			var noLogin = {};
			noLogin.errore = true;
			res.jsonp(noLogin);
			ee.emit("esse3Complete", esse3);
			res1.end;
			return;
		}


		io.to(sessID).emit('update', 8);
		

		var esse3 = {};

		if(DOMsize > 20000){
/***
 *        ▄▄▄▄           ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
 *      ▄█░░░░▌         ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 *     ▐░░▌▐░░▌         ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
 *      ▀▀ ▐░░▌         ▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌
 *         ▐░░▌         ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌
 *         ▐░░▌         ▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 *         ▐░░▌         ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀ 
 *         ▐░░▌         ▐░▌          ▐░▌       ▐░▌▐░▌     ▐░▌  ▐░▌          ▐░▌          ▐░▌     ▐░▌  
 *     ▄▄▄▄█░░█▄▄▄      ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌ 
 *    ▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌
 *     ▀▀▀▀▀▀▀▀▀▀▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀ 
 *                                                                                                    
 */
			

 	
			console.log('1 career student')
			esse3.studente = status.statusStudente(html);
			console.log(esse3.studente.name,'---**--')
			/*
			esse3.name = '';
			esse3.mailAteneo = $('#gu-hpstu-boxDatiPersonali dl:nth-child(1) dd:nth-child(12) description:nth-child(1)').text();
			esse3.appelli = {};
			esse3.appelli.disp = '';
			esse3.appelli.disp = $('#gu-homepagestudente-tablePanelControl tr:nth-child(3) td:nth-child(2)').text().toString();
			var iunica = {}
			iunica.appelli= [];
			iunica.appelli = esse3.appelli.disp.trim().slice(0,2);
			console.log(iunica.appelli)
			esse3.appelli.disp = parseInt(iunica.appelli[0].trim());
			esse3.appelli.pren = $('#gu-homepagestudente-tablePanelControl tr:nth-child(4) td:nth-child(2)').text().toString();
			iunica.pren= [];
			iunica.pren = esse3.appelli.pren.trim().slice(0,2);
			esse3.appelli.pren = parseInt(iunica.pren[0].trim());
			esse3.photo = 'https://webstudenti.unica.it/esse3/' + $('#sottotitolo-menu-principale dl dd img').attr('src');
			console.log($('#gu-homepagestudente-tablePanelControl-cell33-link3').attr('href'))
			esse3.hrefappelli = $('#gu-homepagestudente-tablePanelControl-cell33-link3').attr('href');
			esse3.name = $('#sottotitolo-menu-principale dl dt').text();
			console.log('menu di : ' + esse3.name + ' ***************************************************************');
			*/
			var download = function(uri, filename, callback){
  				request.head(uri, function(err, res, body){   			
    				request(uri, reqBasicA.reqBA(req)).pipe(fs.createWriteStream(filename)).on('close', callback);
    			});
  			};


  			io.to(sessID).emit('update', 13);


  			var esm = esse3.studente.name.replace(/\s/g, '');
			function makeid()
			{
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			    for( var i=0; i < 5; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));

			    return text;
			}
			var c = makeid();
			esm = esm.concat(c);

			esm = esm.concat(".jpg");
			download(esse3.photo, './pictures' + esm, function(){
				console.log('done');
			});
			
			esse3.photo = esm;
			
			//#gu-textStatusStudenteCorsoFac-text-link2
			esse3.careers = [];
			var corso = {};
			corso.titolo = $('#gu-textStatusStudenteCorsoFac-text-link2').text();
			esse3.careers.push(corso)
			console.log($('#gu-textStatusStudenteCorsoFac-text-link2').text());
			
			//#gu-homepagestudente-tablePanelControl tbody:nth-child(1) tr:nth-child(2) td:nth-child(2) img:nth-child(1)
			console.log($('#gu-homepagestudente-tablePanelControl tr:nth-child(2) td:nth-child(2) img').attr('src'));

			//*[@id="menu-tutti"]/ul/li[4]/a
			//*[@id="sottotitolo-menu-principale"]/dl/dt
			//*[@id="menu-tutti"]/ul/li[4]/a
			var link = $('#menu-tutti ul li').find('a').eq(3).attr('href');
			console.log($('#menu-tutti ul li').find('a').eq(3).text());
	
			esse3.logout = "https://webstudenti.unica.it/esse3/auth/"+ $('#menu-open ul:nth-child(2) li:nth-child(1) a').attr('href');
			console.log(esse3.logout);
			
			//#gu-homepagestudente-tablePanelControl > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > img:nth-child(1)
			if ($('#gu-homepagestudente-tablePanelControl tr:nth-child(2) td:nth-child(2) img').attr('src').indexOf(('esito_a'))>=0) {
				console.log("situazione tasse regolare");
				esse3.tax = true;
			} else {
				console.log("situazione non regolare");
				esse3.tax = false;
			}

			io.to(sessID).emit('update', 25);
/***
 *    _________ _______  _______  _______  _______ 
 *    \__   __/(  ___  )(  ____ \(  ____ \(  ____ \
 *       ) (   | (   ) || (    \/| (    \/| (    \/
 *       | |   | (___) || (_____ | (_____ | (__    
 *       | |   |  ___  |(_____  )(_____  )|  __)   
 *       | |   | (   ) |      ) |      ) || (      
 *       | |   | )   ( |/\____) |/\____) || (____/\
 *       )_(   |/     \|\_______)\_______)(_______/
 *                                                 
 */

			//#gu-homepagestudente-tablePanelControl-cell13-link3
			var hreftax = 'https://webstudenti.unica.it/esse3/' + $('#gu-homepagestudente-tablePanelControl-cell13-link3').attr('href');
			request(hreftax, reqBasicA.reqBA(req), function (err, res3, html) {
				if (err) {
					console.log(err);
					return;
				}
				var $ = cheerio.load(html);
				esse3.taxes = [];
				console.log($('tr.tplTitolo:nth-child(2) td:nth-child(1) b:nth-child(1)').text());
				var rawTax = $('table.detail_table tr').map(function(){
					var $row = $(this);
					var t = {};
					t.numero = ($row.children().eq(0).text());
					t.avviso = ($row.children().eq(1).text());
					t.anno = ($row.children().eq(2).text());
					t.desc = ($row.children().eq(3).text());
					t.scadenza = ($row.children().eq(4).text())
					t.importo = ($row.children().eq(5).text())
					console.log($row.children().find('img').attr('src'))
					var semaforo = $row.children().find('img').attr('src');
					console.log(semaforo)
					if ($row.children().find('img').attr('src')) {
									if ($row.children().find('img').attr('src').indexOf(('f_v'))>=0) {
										t.paid = true;
									} 
									else { 
										t.paid = false;
									}
									esse3.taxes.push(t);
								}
				})
			
			
			//#menu-tutti > ul:nth-child(2) > li:nth-child(4) > a:nth-child(1)

			

			request('https://webstudenti.unica.it/esse3/' + $('#menu-tutti ul:nth-child(2) li:nth-child(4) a:nth-child(1)').attr('href'),
				reqBasicA.reqBA(req), function (err, res2, html) {
				if (err) {
					console.log(err);
					return;
				}
				var $ = cheerio.load(html);
				// IMPORTANT!! STORING Student Name in ESSE3.NAME
				esse3.name = new String();
				esse3.photo = new String();
				//*[@id="sottotitolo-menu-principale"]/dl/dd/img
				console.log($('#sottotitolo-menu-principale dl dd img').attr('src'));
				esse3.photo = 'https://webstudenti.unica.it/esse3/' + $('#sottotitolo-menu-principale dl dd img').attr('src');
				esse3.name = $('#sottotitolo-menu-principale dl dt').text();
				console.log('menu di : ' + esse3.name + '***************************************************************');

				//*[@id="menu-tutti"]/ul/li[4]/a
				var link = $('#menu-tutti ul li').find('a').eq(3).attr('href');
				console.log($('#menu-tutti ul li').find('a').eq(3).text());
				
				io.to(sessID).emit('update', 43);
				//3rd request*************************************************************************************
				//************************************************************************************************
			


/***
 *     _______  _______  _______  _______ _________
 *    (  ____ \(  ____ \(  ___  )(       )\__   __/
 *    | (    \/| (    \/| (   ) || () () |   ) (   
 *    | (__    | (_____ | (___) || || || |   | |   
 *    |  __)   (_____  )|  ___  || |(_)| |   | |   
 *    | (            ) || (   ) || |   | |   | |   
 *    | (____/\/\____) || )   ( || )   ( |___) (___
 *    (_______/\_______)|/     \||/     \|\_______/
 *                                                 
 */


			var linkTo = 'https://webstudenti.unica.it/esse3/' + link;
			request(linkTo, reqBasicA.reqBA(req), function (err, res3, html) {
				if (err) {
					console.log(err)
					return
				}
				esse3.libretto = [];
				var $ = cheerio.load(html);
				var rows = $("#esse3old table:nth-child(3) tr:nth-child(4) td table tr").length;
				console.log("contiamo le righe ", rows);


				//Conto quante righe ha la tabella esami e quindi quanti esami presenta il corso
				console.log('le righe della tabella esami sono', rows - 1);
				//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[33]
				
				
				//.titolopagina				
				var matricola = $('.titolopagina').text().toString();
				matricola = matricola.split("/");
				var po = matricola[2];
				esse3.matricola = po.slice(0,po.length-1);
				
				var totCred = 0;
				var counter = 0;
				var offset = 0;
				var ind = 0;
				var storeExam = 0;
				var alive = false;
				var father = '';
				
				for (var i = 1; i < $("#esse3old table:nth-child(3) tr:nth-child(4) td table tr").length; i++) {
					var exam = {};
					exam.module = false;
					var rs = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).attr('rowspan'));
					console.log('questo esame ha un rowspan di : ', rs)

					console.log('counter risulta essere: ', counter)
					if (counter) {
						console.log('sono un modulo')
						exam.module = true;
						var an = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).text());
						console.log(an.trim())
						//var codice = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(1).text());
						//console.log(codice)
						an = an.replace(/[\n\r\t]/g, '')
						console.log('dopo replace ', an)

						an = an.split(" - ");
						console.log(an[0])
						console.log(an[1])
						exam.cod = an[0].trim();
						exam.nomeCorso = an[1].trim();
						console.log('nome corso di un modulo: ', exam.nomeCorso)
						exam.padre = father;
						console.log('nome corso di un modulo: ', exam.nomeCorso, ' padre: ',exam.padre)
						
						
						//table.detail_table > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(6)
						console.log('i: ',i)
						exam.crediti = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(5).text(), 10);
						console.log('*******************************',exam.crediti);
						
						var altImage = $('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(6).children("img").attr('alt');
						if (altImage == 'Superata') {
							exam.passed = true;
							totCred += exam.crediti;
							} else exam.passed = false;
											
						exam.frequenza = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(7).text());	
						var registrazione = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(8).text());
					//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[10]
					var arraysplit = registrazione.replace(/\s/g, '').split("-");

					if (arraysplit[0])
						exam.voto = arraysplit[0].trim();
					if (arraysplit[1])
					exam.dataReg = arraysplit[1].trim();


						esse3.libretto.push(exam);

						if(!(counter)) father = '';
						counter--;
						continue;

					}


					exam.id = i;
					exam.anno = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).text());
					var codice = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(1).text());
					console.log('codice = ',codice)
					codice = codice.split(" - ");
					
					console.log(codice[0])
					exam.cod = codice[0].trim();
					exam.nomeCorso = codice[1].trim();
					
					exam.crediti = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(6).text(), 10);
					//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[4]/td[8]
					if(!(isNaN(rs))) {
								exam.crediti = 0;
								console.log('****RS: ',rs,'**************', exam.nomeCorso,'*******************************',exam.crediti);
							}
					console.log('crediti esame normale: ', exam.crediti)
					var altImage = $('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(7).children("img").attr('alt');
					if (altImage == 'Superata') {
						exam.passed = true;
						totCred += exam.crediti;
					} else exam.passed = false;
					//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[9]
					exam.frequenza = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(8).text());
					var registrazione = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(9).text());
					//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[10]
					var arraysplit = registrazione.replace(/\s/g, '').split("-");

					if (arraysplit[0])
						exam.voto = arraysplit[0].trim();
					if (arraysplit[1])
					exam.dataReg = arraysplit[1].trim();
					console.log('*******************************',exam.crediti);


					if(!(isNaN(rs))) {

						console.log('i successivi ', (rs-1), 'esami sono moduli')
						console.log('sono dove imposto crediti zero al padre')
						counter = rs-1;
						console.log('counter ', counter)
						father = exam.nomeCorso;
						exam.crediti = 0;
					}



					esse3.libretto.push(exam);
					
					
				}
				console.log("Totale crediti : ", totCred);
				esse3.totCrediti = totCred;

				//*[@id="esse3old"]/table[3]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[1]/td
				var mediaA = $("#esse3old table:nth-child(3) tr:nth-child(2) td table tr td table tr td").text();
				mediaA = mediaA.split("/");
				esse3.mediaArit = mediaA[0].trim();
				//*[@id="esse3old"]/table[3]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td
				var mediaP = $("#esse3old table:nth-child(3) tr:nth-child(2) td table tr td table tr:nth-child(2) td").text();
				mediaP = mediaP.split("/");
				esse3.mediaPond = mediaP[0].trim();
				console.log("Media Ponderata : " + esse3.mediaPond);
				console.log("Media Aritmetica  : " + esse3.mediaArit);
				//*[@id="menu-open"]/ul/li[1]/a
				var logout = $('#menu-open ul li:nth-child(1) a').attr('href');

//#menu-tutti > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)
				var linkCertificati = 'https://webstudenti.unica.it/esse3/' + $('#menu-tutti ul:nth-child(2) li:nth-child(2) a:nth-child(1)').attr('href');
				

				var studente = new Studente(esse3);
				studente.save(function (err) {
					if (err) {
						console.log(err);
						return;
					}
					res.jsonp(esse3);
					ee.emit("esse3Complete", esse3);
				});
			});
			});
		})
		}
		else
/***
 *     ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
 *    ▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 *    ▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀      ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
 *    ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌               ▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          
 *    ▐░▌ ▐░▐░▌ ▐░▌▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄      ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
 *    ▐░▌  ▐░▌  ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 *    ▐░▌   ▀   ▐░▌▐░▌       ▐░▌▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀      ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
 *    ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌     ▐░▌  ▐░▌               ▐░▌          ▐░▌       ▐░▌▐░▌     ▐░▌  ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
 *    ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄      ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
 *    ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 *     ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
 *                                                                                                                                                        
 */			
  			

		{
			var esse3 = {};
			esse3.careers = [];
			var	$ = cheerio.load(html);
			
			
			for (var i = 1; i < $("#esse3old table:nth-child(3) tr:nth-child(3) td table tr").length; i++) {
				//*[@id="esse3old"]/table[3]/tbody/tr[3]/td/table/tbody/tr[2]/td[1]
				var course = {};
				console.log(i);
				try {
					course.cod = ($('#esse3old table:nth-child(3) tr:nth-child(3) td table tr').eq(i).find('td').eq(0).text());
					course.tipo = ($('#esse3old table:nth-child(3) tr:nth-child(3) td table tr').eq(i).find('td').eq(1).text());
					course.titolo = ($('#esse3old table:nth-child(3) tr:nth-child(3) td table tr').eq(i).find('td').eq(2).text());
					course.stato = ($('#esse3old table:nth-child(3) tr:nth-child(3) td table tr').eq(i).find('td').eq(3).text());
					course.link = ($('#esse3old table:nth-child(3) tr:nth-child(3) td table tr').eq(i).find('a').attr('href'));
					esse3.careers.push(course);
				} catch (e) {
					console.log(e)
					console.log('in the catch')
					res.jsonp({ messaggio: 'Login e/o password errati'})
				}
			}

			if(!(esse3.careers)) {
				res.jsonp({errore: true})
			};


			io.to(sessID).emit('update', 16);

			console.log('carriere caricate \n', esse3.careers);
			
			request('https://webstudenti.unica.it/esse3/' + esse3.careers[0].link, reqBasicA.reqBA(req), 
				function (err, res2, html) {
					if (err) {
						console.log(err);
						return;
					}
			var $ = cheerio.load(html);
			// IMPORTANT!! STORING Student Name in ESSE3.NAME
			esse3.name = '';
			esse3.photo = '';
			esse3.studente = status.statusStudente(html);
			
			var scheda = {};

			scheda.residenza = $('#gu-hpstu-boxDatiPersonali dl:nth-child(1) dd:nth-child(6)').text();
			scheda.residenza = scheda.residenza.split('\r');
			vv = scheda.residenza[0].split(',');
			scheda.via = vv[0];
			scheda.cap = vv[1];
			tt = vv[1].split("tel:");
			scheda.tel = tt[1];
			scheda.ttzero = tt[0];
			ww = tt[0].split(" ");
			scheda.po = ww;
			//scheda.residenza.tel = tt[]
			//scheda.tel = scheda.residenza.split('tel:');
			esse3.studente.res = scheda;
			console.log($('#gu-homepagestudente-tablePanelControl-cell33-link3').attr('href'))
			esse3.hrefappelli = $('#gu-homepagestudente-tablePanelControl-cell33-link3').attr('href');


			esse3.mailAteneo = $('#gu-hpstu-boxDatiPersonali dl:nth-child(1) dd:nth-child(12) description:nth-child(1)').text();
			esse3.logout = "https://webstudenti.unica.it/esse3/auth/"+ $('#menu-open ul:nth-child(2) li:nth-child(1) a').attr('href');


			//	#gu-homepagestudente-tablePanelControl > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2)
			esse3.appelli = {};
			esse3.appelli.disp = '';
			esse3.appelli.disp = $('#gu-homepagestudente-tablePanelControl tr:nth-child(3) td:nth-child(2)').text().toString();
			console.log(esse3.appelli.disp);
			var iunica = {}
			iunica.appelli= [];
			iunica.appelli = esse3.appelli.disp.trim().slice(0,2);
			esse3.appelli.disp = parseInt(iunica.appelli[0].trim());
				
				//#gu-homepagestudente-tablePanelControl > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2)
			esse3.appelli.pren = $('#gu-homepagestudente-tablePanelControl tr:nth-child(4) td:nth-child(2)').text().toString();
			iunica.pren= [];
			iunica.pren = esse3.appelli.pren.trim().slice(0,2);
			esse3.appelli.pren = parseInt(iunica.pren[0].trim());
			
		
			io.to(sessID).emit('update', 27);


				//*[@id="sottotitolo-menu-principale"]/dl/dd/img
			esse3.name = $('#sottotitolo-menu-principale dl dt').text();
			console.log($('#sottotitolo-menu-principale dl dd img').attr('src'));
			esse3.photo = 'https://webstudenti.unica.it/esse3/' + $('#sottotitolo-menu-principale dl dd img').attr('src');
			console.log('menu di : ' + esse3.name + '***************************************************************');
		
				
				var download = function(uri, filename, callbck){
  				request.head(uri, function(err, res, body){   			
    				request(uri, {
						jar: request.jar(),
						auth: {
							user: req.params.user,
							pass: req.params.pass,
							sendImmediately: false
						},
						headers: {
							'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0'
						}
					}).pipe(fs.createWriteStream(filename)).on('close', callbck);
  				});
				};


				io.to(sessID).emit('update', 34);

				var esm = esse3.studente.name.replace(/\s/g, '');
				function makeid()
				{
				    var text = "";
				    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				    for( var i=0; i < 5; i++ )
				        text += possible.charAt(Math.floor(Math.random() * possible.length));

				    return text;
				}
				var c = makeid();
				esm = esm.concat(c);

				esm = esm.concat(".jpg");
				download(esse3.photo, './pictures/' + esm, function(){
  				console.log('done');
				});
				
				esse3.photo = esm;

			//*[@id="gu-homepagestudente-tablePanelControl"]/tbody/tr[2]/td[2]/img
			console.log($('#gu-homepagestudente-tablePanelControl tr:nth-child(2) td:nth-child(2) img').attr('src'))
			if ($('#gu-homepagestudente-tablePanelControl tr:nth-child(2) td:nth-child(2) img').attr('src').indexOf(('esito_a'))>=0) {
				console.log("situazione tasse regolare")
				esse3.tax = true;
			} else { 
				console.log("situazione non regolare");
				esse3.tax = false
			}
			
			//*[@id="menu-tutti"]/ul/li[4]/a
			//*[@id="sottotitolo-menu-principale"]/dl/dt
			//*[@id="menu-tutti"]/ul/li[4]/a
			var link = $('#menu-tutti ul li').find('a').eq(3).attr('href');
			console.log($('#menu-tutti ul li').find('a').eq(3).text());
			//#gu-homepagestudente-tablePanelControl-cell13-link3
				
			var hreftax = $('#gu-homepagestudente-tablePanelControl-cell13-link3').attr('href');
			console.log(hreftax);
						request('https://webstudenti.unica.it/esse3/' + hreftax, {
								jar: request.jar(),
								auth: {
									user: req.params.user,
									pass: req.params.pass,
									sendImmediately: false
								},
								headers: {
									'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0'
								}
							}, function (err, res3, html) {
								if (err) {
									console.log(err)
									return
								}
							//esse3.taxes = new Object();
						//	esse3.taxes.unpaid = new Array();
							//esse3.taxes.paid = new Array();
							//tr.tplTitolo:nth-child(2) > td:nth-child(1)
							//tr.tplTitolo:nth-child(2) > td:nth-child(1) > b:nth-child(1)
							var $ = cheerio.load(html);
							esse3.taxes = [];
							var rawTax = $('table.detail_table tr').map(function(){
								var $row = $(this);
								var t = {};
								t.numero = ($row.children().eq(0).text());
								t.avviso = ($row.children().eq(1).text());
								t.anno = ($row.children().eq(2).text());
								t.desc = ($row.children().eq(3).text());
								t.scadenza = ($row.children().eq(4).text());
								t.importo = ($row.children().eq(5).text());
								//t.img = ($row.children().find('img').attr('src'));
								if ($row.children().find('img').attr('src')) {
									if ($row.children().find('img').attr('src').indexOf(('f_v'))>=0) {
										t.paid = true;
									} 
									else { 
										t.paid = false;
									}
									esse3.taxes.push(t);
								}
							})
					io.to(sessID).emit('update', 51);
					
					
/***
 *     _______  _______  _______  _______ _________
 *    (  ____ \(  ____ \(  ___  )(       )\__   __/
 *    | (    \/| (    \/| (   ) || () () |   ) (   
 *    | (__    | (_____ | (___) || || || |   | |   
 *    |  __)   (_____  )|  ___  || |(_)| |   | |   
 *    | (            ) || (   ) || |   | |   | |   
 *    | (____/\/\____) || )   ( || )   ( |___) (___
 *    (_______/\_______)|/     \||/     \|\_______/
 *                                                 
 */
					
					io.to(sessID).emit('update', 72);
					request('https://webstudenti.unica.it/esse3/' + link, 
						reqBasicA.reqBA(req), function (err, res3, html) {
						if (err) {
							console.log(err);
							return
						}
						esse3.libretto = [];
						var $ = cheerio.load(html);
						var rows = $("#esse3old table:nth-child(3) tr:nth-child(4) td table tr").length;
						console.log("contiamo le righe ", rows);


						//Conto quante righe ha la tabella esami e quindi quanti esami presenta il corso
						console.log('le righe della tabella esami sono', rows - 1);
						//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[33]

						//.titolopagina				
						var matricola = $('.titolopagina').text().toString();
						matricola = matricola.split("/");
						var po = matricola[2];
						esse3.matricola = po.slice(0,po.length-1);


						io.to(sessID).emit('update', 88);
						
						var totCred = 0;

						var totCred = 0;
						var counter = 0;
						var offset = 0;
						var ind = 0;
						var storeExam = 0;
						var alive = false;
						var father = '';
						for (var i = 1; i < $("#esse3old table:nth-child(3) tr:nth-child(4) td table tr").length; i++) {
							var exam = {};

							exam.module = false;
							var rs = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).attr('rowspan'));
							
							console.log('questo esame ha un rowspan di : ', rs)

							console.log('counter risulta essere: ', counter)

							if (counter) {
								console.log('sono un modulo')
								exam.module = true;
								var an = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).text());
								console.log(an.trim())
								//var codice = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(1).text());
								//console.log(codice)
								an = an.replace(/[\n\r\t]/g, '')
								console.log('dopo replace ', an)

								an = an.split(" - ");
								console.log(an[0])
								console.log(an[1])
								exam.cod = an[0].trim();
								exam.nomeCorso = an[1].trim();
								console.log('nome corso di un modulo: ', exam.nomeCorso)
								exam.padre = father;
								console.log('nome corso di un modulo: ', exam.nomeCorso, ' padre: ',exam.padre)
								
								
								//table.detail_table > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(6)
								console.log('i: ',i)
								exam.crediti = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(5).text(), 10);
								console.log('*******************************',exam.crediti);
								var altImage = $('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(6).children("img").attr('alt');
								if (altImage == 'Superata') {
									exam.passed = true;
									totCred += exam.crediti;
									} else exam.passed = false;
													
								exam.frequenza = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(7).text());	
								var registrazione = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(8).text());
							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[10]
							var arraysplit = registrazione.replace(/\s/g, '').split("-");

							if (arraysplit[0])
								exam.voto = arraysplit[0].trim();
							if (arraysplit[1])
							exam.dataReg = arraysplit[1].trim();


								esse3.libretto.push(exam);

								if(!(counter)) father = '';
								counter--;
								continue;

							}


							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[1]
							exam.id = i;
							exam.anno = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(0).text());

							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[2]
							var codice = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(1).text());
							codice = codice.split(" - ");
							exam.cod = codice[0].trim();
							exam.nomeCorso = codice[1].trim();

							exam.crediti = parseInt($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(6).text(), 10);
							
							console.log('****RS: ',rs,'**************', exam.nomeCorso,'*******************************',exam.crediti);
							if(!(isNaN(rs))) {
								exam.crediti = 0;
								console.log('****RS: ',rs,'**************', exam.nomeCorso,'*******************************',exam.crediti);
							}
							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[4]/td[8]
							var altImage = $('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(7).children("img").attr('alt');
							if (altImage == 'Superata') {
								exam.passed = true;
								totCred += exam.crediti;
							} else exam.passed = false;
							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[9]
							exam.frequenza = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(8).text());
							var registrazione = ($('#esse3old table:nth-child(3) tr:nth-child(4) td table tr').eq(i).find('td').eq(9).text());
							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[10]
							var boh = registrazione.replace(/\s/g, '').split("-");

							if (boh[0])
								exam.voto = boh[0].trim();
							if (boh[1])
								exam.dataReg = boh[1].trim();

							
							if(!(isNaN(rs))) {
								console.log('i successivi ', (rs-1), 'esami sono moduli')
								console.log('sono dove imposto crediti zero al padre')

								counter = rs-1;
								console.log('counter ', counter)
								father = exam.nomeCorso;
								exam.crediti = 0;
								console.log('++++++++++++++++++++++++++++++++++++++++++++++',exam.crediti)
							}
							esse3.libretto.push(exam);
							//console.log(exam);
							//*[@id="esse3old"]/table[3]/tbody/tr[4]/td/table/tbody/tr[2]/td[8]/img
						}
						console.log("Totale crediti : ", totCred);
						esse3.totCrediti = totCred;

						io.to(sessID).emit('update', 100);

						//*[@id="esse3old"]/table[3]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[1]/td
						var mediaA = $("#esse3old table:nth-child(3) tr:nth-child(2) td table tr td table tr td").text();
						mediaA = mediaA.split("/");
						esse3.mediaArit = mediaA[0].trim();
						//*[@id="esse3old"]/table[3]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td
						var mediaP = $("#esse3old table:nth-child(3) tr:nth-child(2) td table tr td table tr:nth-child(2) td").text();
						mediaP = mediaP.split("/");
						esse3.mediaPond = mediaP[0].trim();
						console.log("Media Ponderata : " + esse3.mediaPond);
						console.log("Media Aritmetica  : " + esse3.mediaArit);
						//*[@id="menu-open"]/ul/li[1]/a
						var logout = $('#menu-open ul li:nth-child(1) a').attr('href');
						var studente = new Studente(esse3);
						studente.save(function (err) {
							if (err) {
								console.log(err);
								return;
							}
							res.jsonp(esse3);
							ee.emit("esse3Complete", esse3);
						});
					});
				});
			});
		};
	});
})
					 
router.get('/:user/:pass/appelli/:auth/:stud/:app/:end', function (req, res, next) {
	console.log('route APPELLI')
	var ur = encodeURI(req.params.auth + '/' + req.params.stud + '/' + req.params.app + '/' + req.params.end);
	console.log(ur)
	var z = {};
	request('https://webstudenti.unica.it/esse3/' + ur, 
			reqBasicA.reqBA(req), function (err, res1, html) {
						if (err) {
							console.log(err);
							return
						}
						console.log('chiedo gli appelli')
						var $ = cheerio.load(html);
						console.log('dom caricato')
						console.log($('.titolopagina').text());
						console.log($('table.detail_table tr:nth-child(2) td:nth-child(2)').text());
						z.appello = $('table.detail_table tr:nth-child(2) td:nth-child(2)').text();
						console.log(z.appello)
						//res1.jsonp(z)

						})
		console.log('ok ', req.params)
})

router.get('/:user/:pass/prova/:auth/:stud/:app/:end', function (req, res, next) {
	console.log('route PROVA')
	var ur = encodeURI(req.params.auth + '/' + req.params.stud + '/' + req.params.app + '/' + req.params.end);
	console.log(ur)
	var z = {};
	z.appelli = [];
	request('https://webstudenti.unica.it/esse3/' + ur, reqBasicA.reqBA(req), function (err, res1, html) {
		if (err) { 
			console.log(err); 
			return; 
		}
		console.log('vado in prova')
		var $ = cheerio.load(html);
		console.log('dom caricato');
		console.log($('.titolopagina').text());
		var recuperaAppelli = $('table.detail_table tr').map(function(){
			var $row = $(this);
			var ap = {};
			
			ap.corso = ($row.children().eq(1).text());
			ap.daytest = ($row.children().eq(2).text());
			ap.range = ($row.children().eq(3).text());
			
			ap.daystart = ap.range.slice(0,10)
			ap.dayend = ap.range.slice(10,20)
			ap.desc = ($row.children().eq(4).text())
			ap.prof = ($row.children().eq(5).text())
			ap.cfu = ($row.children().eq(6).text())
			if($row.children().find('img').attr('src') == undefined ) { 
				ap.href = $row.children().find('form').attr('action')
				console.log(ap.href)
				console.log('esame prenotabile : true');
				ap.prenotabile = true;
				z.appelli.push(ap);
			} else {
				console.log('esame prenotabile : false');
				ap.prenotabile = false;
				
				z.appelli.push(ap);
			}
		})
		console.log(toType.toType(z))
		console.log(toType.toType(z.appelli))
		z.appelli.shift();
		res.jsonp(z);
		})
	console.log('ok ', req.params)
})


router.get('/fake', function(req,res,next){
	var test = {"careers":[{"cod":"70/73/43608","tipo":"Corso di Laurea","titolo":"INGEGNERIA PER L'AMBIENTE E IL TERRITORIO","stato":"Attivo","link":"auth/studente/SceltaCarrieraStudente.do;jsessionid=7DF994E85EB56367CF0F0A970A51CD52?stu_id=256293"},{"cod":"60/60/45829","tipo":"Corso di Laurea","titolo":"FISICA","stato":"Cessato - Rinuncia","link":"auth/studente/SceltaCarrieraStudente.do;jsessionid=7DF994E85EB56367CF0F0A970A51CD52?stu_id=246489"}],"name":"SIMONE PAU","photo":"SIMONEPAU6TJNF.jpg","studente":{"name":"SIMONE PAU","photo":"https://webstudenti.unica.it/esse3/img/personalizzazione/ico-studente.gif","mailAteneo":"si.pau2@studenti.unica.it","mailPersonale":"","logout":"https://webstudenti.unica.it/esse3/auth/Logout.do;jsessionid=7DF994E85EB56367CF0F0A970A51CD52","appelli":{"disp":1,"pren":0},"aa":"2014/2015","annoRegolamento":"2011","statoCarriera":"attivo","corso":"INGEGNERIA PER L'AMBIENTE E IL TERRITORIO","facolta":"INGEGNERIA E ARCHITETTURA ","percorso":"PERCORSO COMUNE - 73/00","durata":"3 anni","annoDiCorso":"3°(1°fuori corso)","immatricolazione":"16/09/2011","esamiRegistrati":"29","mediaAritmetica":"26.471","mediaPonderata":"26.18","res":{"residenza":["Via Benedetto XV , 2209032 Assemini tel:070942583","\n","\n","\nmodifica","\n","\n"],"via":"Via Benedetto XV ","cap":" 2209032 Assemini tel:070942583","tel":"070942583","ttzero":" 2209032 Assemini ","po":[" 2209032 Assemini "]}},"hrefappelli":"auth/studente/Appelli/Appelli.do;jsessionid=7DF994E85EB56367CF0F0A970A51CD52","mailAteneo":"si.pau2@studenti.unica.it","logout":"https://webstudenti.unica.it/esse3/auth/Logout.do;jsessionid=7DF994E85EB56367CF0F0A970A51CD52","appelli":{"disp":1,"pren":0},"tax":false,"taxes":[{"numero":"5072687","avviso":"201509462140","anno":"14/15","desc":"Tassa di iscrizione/immatricolazione","scadenza":"23/02/2015","importo":"€ 63,00","paid":false},{"numero":"5009447","avviso":"201503910747","anno":"14/15","desc":"Tassa di iscrizione/immatricolazione","scadenza":"06/10/2014","importo":"€ 227,44","paid":true},{"numero":"4943613","avviso":"201415064679","anno":"13/14","desc":"Tassa di iscrizione/immatricolazione","scadenza":"30/06/2014","importo":"€ 152,71","paid":true},{"numero":"4868778","avviso":"201410450061","anno":"13/14","desc":"Tassa di iscrizione/immatricolazione","scadenza":"03/02/2014","importo":"€ 62,06","paid":true},{"numero":"4784077","avviso":"201402776248","anno":"13/14","desc":"Tassa di iscrizione/immatricolazione","scadenza":"04/10/2013","importo":"€ 224,41","paid":true},{"numero":"4604785","avviso":"201305098420","anno":"12/13","desc":"Tassa di iscrizione/immatricolazione","scadenza":"05/10/2012","importo":"€ 14,62","paid":true},{"numero":"4378295","avviso":"201206602891","anno":"11/12","desc":"Tassa di iscrizione/immatricolazione","scadenza":"05/10/2011","importo":"€ 14,62","paid":true},{"numero":"4348747","avviso":"201204209559","anno":"11/12","desc":"Tassa per la partecipazione alle prove di ammissione ai Corsi di studio","scadenza":"31/08/2011","importo":"€ 21,00","paid":true},{"numero":"4348704","avviso":"201204205216","anno":"11/12","desc":"Tassa per la partecipazione alle prove di ammissione ai Corsi di studio","scadenza":"31/08/2011","importo":"€ 21,00","paid":true},{"numero":"4263548","avviso":"201113927655","anno":"10/11","desc":"Tassa di iscrizione/immatricolazione","scadenza":"02/05/2011","importo":"€ 146,74","paid":true},{"numero":"4215444","avviso":"201112365760","anno":"10/11","desc":"Tassa di iscrizione/immatricolazione","scadenza":"01/02/2011","importo":"€ 59,35","paid":true},{"numero":"4145683","avviso":"201105908732","anno":"10/11","desc":"Tassa di iscrizione/immatricolazione","scadenza":"23/09/2010","importo":"€ 210,18","paid":true},{"numero":"4126439","avviso":"201104143752","anno":"10/11","desc":"Tassa per la partecipazione alle prove di ammissione ai Corsi di studio","scadenza":"27/08/2010","importo":"€ 20,68","paid":true},{"numero":"4126437","avviso":"201104143550","anno":"10/11","desc":"Tassa per la partecipazione alle prove di ammissione ai Corsi di studio","scadenza":"27/08/2010","importo":"€ 20,68","paid":true},{"numero":"4126429","avviso":"201104142742","anno":"10/11","desc":"Tassa per la partecipazione alle prove di ammissione ai Corsi di studio","scadenza":"27/08/2010","importo":"€ 20,68","paid":true}],"libretto":[{"module":false,"id":1,"anno":"1","cod":"70/0001-M","nomeCorso":"CHIMICA","crediti":6,"passed":true,"frequenza":"2011/2012","voto":"29","dataReg":"29/02/2012"},{"module":false,"id":2,"anno":"1","cod":"70/0002-M","nomeCorso":"CORSO INTEGRATO: MATEMATICA 1","crediti":12,"passed":true,"frequenza":"2011/2012","voto":"19","dataReg":"25/09/2012"},{"module":false,"id":3,"anno":"1","cod":"70/0003-M","nomeCorso":"ECONOMIA APPLICATA ALL'INGEGNERIA","crediti":5,"passed":true,"frequenza":"2013/2014","voto":"27","dataReg":"09/07/2014"},{"module":false,"id":4,"anno":"1","cod":"70/0004-M","nomeCorso":"FISICA 1","crediti":8,"passed":true,"frequenza":"2011/2012","voto":"30","dataReg":"14/06/2012"},{"module":false,"id":5,"anno":"1","cod":"70/0005-M","nomeCorso":"FISICA 2","crediti":7,"passed":true,"frequenza":"2011/2012","voto":"24","dataReg":"20/07/2012"},{"module":false,"id":6,"anno":"1","cod":"70/0006-M","nomeCorso":"FONDAMENTI DI INFORMATICA 1","crediti":6,"passed":false,"frequenza":" "},{"module":false,"id":7,"anno":"1","cod":"70/0008-M","nomeCorso":"MATEMATICA 2","crediti":9,"passed":true,"frequenza":"2013/2014","voto":"20","dataReg":"15/07/2014"},{"module":false,"id":8,"anno":"1","cod":"70/0010-M","nomeCorso":"PROVA DI CONOSCENZA DI LINGUA INGLESE","crediti":3,"passed":true,"frequenza":"2012/2013","voto":"IDO","dataReg":"14/01/2014"},{"module":false,"id":9,"anno":"2","cod":"IN/0096","nomeCorso":"CORSO INTEGRATO: GEOLOGIA E GEOLOGIA APPLICATA","crediti":0,"passed":true,"frequenza":"2012/2013","voto":"27","dataReg":"31/01/2014"},{"module":true,"cod":"IN/0097","nomeCorso":"GEOLOGIA APPLICATA","padre":"CORSO INTEGRATO: GEOLOGIA E GEOLOGIA APPLICATA","crediti":5,"passed":true,"frequenza":"2012/2013","voto":"30","dataReg":"31/01/2014"},{"module":true,"cod":"IN/0098","nomeCorso":"LITOLOGIA E GEOLOGIA","padre":"CORSO INTEGRATO: GEOLOGIA E GEOLOGIA APPLICATA","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"24","dataReg":"26/09/2013"},{"module":false,"id":12,"anno":"2","cod":"IN/0084","nomeCorso":"CORSO INTEGRATO: STATISTICA E IDROLOGIA","crediti":0,"passed":true,"frequenza":"2012/2013","voto":"26","dataReg":"24/09/2013"},{"module":true,"cod":"IN/0085","nomeCorso":"IDROLOGIA","padre":"CORSO INTEGRATO: STATISTICA E IDROLOGIA","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"26","dataReg":"24/09/2013"},{"module":true,"cod":"IN/0086","nomeCorso":"STATISTICA","padre":"CORSO INTEGRATO: STATISTICA E IDROLOGIA","crediti":4,"passed":true,"frequenza":"2012/2013","voto":"25","dataReg":"18/02/2013"},{"module":false,"id":15,"anno":"2","cod":"70/0064-M","nomeCorso":"LABORATORIO DI DISEGNO","crediti":5,"passed":true,"frequenza":"2011/2012","voto":"IDO","dataReg":"20/02/2013"},{"module":false,"id":16,"anno":"2","cod":"70/0065-M","nomeCorso":"LABORATORIO DI ELETTROTECNICA","crediti":5,"passed":true,"frequenza":"2012/2013","voto":"IDO","dataReg":"25/06/2013"},{"module":false,"id":17,"anno":"2","cod":"70/0077-M","nomeCorso":"PIANIFICAZIONE TERRITORIALE","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"26","dataReg":"29/07/2013"},{"module":false,"id":18,"anno":"2","cod":"IN/0009","nomeCorso":"PRINCIPI DEL TRATTAMENTO DEI SOLIDI","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"26","dataReg":"19/06/2013"},{"module":false,"id":19,"anno":"2","cod":"IN/0008","nomeCorso":"TERMODINAMICA E MACCHINE","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"28","dataReg":"07/03/2013"},{"module":false,"id":20,"anno":"2","cod":"70/0089-M","nomeCorso":"TOPOGRAFIA E CARTOGRAFIA","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"24","dataReg":"20/06/2013"},{"module":false,"id":21,"anno":"2","cod":"0000055556","nomeCorso":"WORKSHOP GEOGRAPHIC INF. SYSTEMS SUPP.DECSION MALKING","crediti":3,"passed":true,"frequenza":"2012/2013","voto":"IDO","dataReg":"03/07/2013"},{"module":false,"id":22,"anno":"3","cod":"IN/0099","nomeCorso":"CORSO INTEGRATO: GEOTECNICA E SISMICA APPLICATA","crediti":0,"passed":true,"frequenza":"2013/2014","voto":"27","dataReg":"16/09/2014"},{"module":true,"cod":"IN/0094","nomeCorso":"GEOTECNICA","padre":"CORSO INTEGRATO: GEOTECNICA E SISMICA APPLICATA","crediti":6,"passed":true,"frequenza":"2013/2014","voto":"26","dataReg":"16/09/2014"},{"module":true,"cod":"IN/0100","nomeCorso":"SISMICA APPLICATA","padre":"CORSO INTEGRATO: GEOTECNICA E SISMICA APPLICATA","crediti":5,"passed":true,"frequenza":"2013/2014","voto":"29","dataReg":"05/06/2014"},{"module":false,"id":25,"anno":"3","cod":"IN/0101","nomeCorso":"CORSO INTEGRATO: SCIENZA E TECNICA DELLE COSTRUZIONI","crediti":0,"passed":true,"frequenza":"2013/2014","voto":"30","dataReg":"20/10/2014"},{"module":true,"cod":"IN/0102","nomeCorso":"SCIENZA DELLE COSTRUZIONI","padre":"CORSO INTEGRATO: SCIENZA E TECNICA DELLE COSTRUZIONI","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"30","dataReg":"25/02/2014"},{"module":true,"cod":"IN/0103","nomeCorso":"TECNICA DELLE COSTRUZIONI","padre":"CORSO INTEGRATO: SCIENZA E TECNICA DELLE COSTRUZIONI","crediti":5,"passed":true,"frequenza":"2013/2014","voto":"29","dataReg":"20/10/2014"},{"module":false,"id":28,"anno":"3","cod":"70/0054-M","nomeCorso":"FENOMENI DI TRASPORTO IN SISTEMI AMBIENTALI","crediti":6,"passed":true,"frequenza":"2013/2014","voto":"30","dataReg":"20/01/2015"},{"module":false,"id":29,"anno":"3","cod":"70/0059-M","nomeCorso":"IDRAULICA","crediti":10,"passed":false,"frequenza":" "},{"module":false,"id":30,"anno":"3","cod":"IN/0010","nomeCorso":"INGEGNERIA SANITARIA AMBIENTALE","crediti":7,"passed":true,"frequenza":"2013/2014","voto":"27","dataReg":"12/12/2014"},{"module":false,"id":31,"anno":"3","cod":"70/0079-M","nomeCorso":"PROVA FINALE","crediti":4,"passed":false,"frequenza":" "},{"module":false,"id":32,"anno":"3","cod":"70/0081-M","nomeCorso":"SICUREZZA DEL LAVORO E DIFESA AMBIENTALE","crediti":6,"passed":true,"frequenza":"2012/2013","voto":"30L","dataReg":"15/01/2014"},{"module":false,"id":33,"anno":"3","cod":"70/0087-M","nomeCorso":"TECNOLOGIE DI CHIMICA APPLICATA","crediti":9,"passed":false,"frequenza":" "}],"matricola":"43608","totCrediti":149,"mediaArit":"26,471","mediaPond":"26,18"}
	res.json(test);
})

router.get('/numCPUs', function(req,res,next){
	console.log(numCPUs);
	res.json(numCPUs);
})


// REGISTER ALL THE ROUTES
// all of our routes will be prefixed with /api
app.use('/api', router);

app.get('*', function(req, res) {
    res.sendfile('./admin/views/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('/', function(req, res){
	ee.emit('sessionID');
	sessID = req.sessionID;
	console.log('da app.get',req.sessionID);
	console.log('sessID',sessID);
	res.sendFile(__dirname + '/index.html');
})

/*
app.get('*', function(req, res, next) {
  var err = new Error();
  err.status = 404;
  next(err);
});
*/
app.use(function(err, req, res, next){
  console.error(err.stack);
  res.status(500).send('Something broke!');
});
console.log('iUnica scraper listening on port :', port);
exports = module.exports = app;